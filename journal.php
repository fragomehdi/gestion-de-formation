<?php
/**
 * Une page de démonstration vide
 * Le contenu de la variable $headerContent sera effecter a header.php pour lui frounir des ressources javascript et CSS
 * Le contenu de la variable $footerContent sera effecter a footer.php pour lui frounir des ressources javascript et CSS
 ***** NE PAS SUPPRIMER OU MODIFIER CE FICHIER ****
 */
?>

<?php
$menuActuel = "menu_parametrage";
$sousMenuActuel = "menu_parametrage_journal";


$headerContent = <<<EOF
EOF;
$footerContent = <<<EOF
<script src="js/colorpicker.js"></script>
<script src="js/bootstrap-timepicker.min.js"></script>

<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/jquery-ui-1.10.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.sparkline.min.js"></script>
<script src="js/toggles.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.cookies.js"></script>

<script src="js/jquery.autogrow-textarea.js"></script>
<script src="js/bootstrap-fileupload.min.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/jquery.tagsinput.min.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/chosen.jquery.min.js"></script>
<script src="js/dropzone.min.js"></script>
<script src="js/colorpicker.js"></script>


<script src="js/jquery.validate.min.js"></script>

<script src="js/custom.js"></script>

<script>
jQuery(document).ready(function(){
    
  // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
  
  // Tags Input
  jQuery('#tags').tagsInput({width:'auto'});
   
  // Textarea Autogrow
  jQuery('#autoResizeTA').autogrow();
  
  // Color Picker
  if(jQuery('#colorpicker').length > 0) {
     jQuery('#colorSelector').ColorPicker({
            onShow: function (colpkr) {
                jQuery(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                jQuery(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                jQuery('#colorSelector span').css('backgroundColor', '#' + hex);
                jQuery('#colorpicker').val('#'+hex);
            }
     });
  }
  
  // Color Picker Flat Mode
    jQuery('#colorpickerholder').ColorPicker({
        flat: true,
        onChange: function (hsb, hex, rgb) {
            jQuery('#colorpicker3').val('#'+hex);
        }
    });
   
  // Date Picker
  jQuery('#datepicker').datepicker();
  
  jQuery('#datepicker-inline').datepicker();
  
  jQuery('#datepicker-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });



// Date Picker
  jQuery('#datepicker').datepicker();
  
  jQuery('#datepicker-inline').datepicker();
  
  jQuery('#datepicker-multiplee').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });

  
  // Spinner
  var spinner = jQuery('#spinner').spinner();
  spinner.spinner('value', 0);
  
  // Input Masks
  jQuery("#date").mask("99/99/9999");
  jQuery("#phone").mask("(999) 999-9999");
  jQuery("#ssn").mask("999-99-9999");
  
  // Time Picker
  jQuery('#timepicker').timepicker({defaultTIme: false});
  jQuery('#timepicker2').timepicker({showMeridian: false});
  jQuery('#timepicker3').timepicker({minuteStep: 15});

  
});




</script>







<script>
jQuery(document).ready(function() {

    jQuery(".nav-parent > a#{$menuActuel}").trigger("click");
    jQuery(".nav-parent > a#{$menuActuel}").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#{$sousMenuActuel}").addClass("active");

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });

    // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});


  // Basic Form
  jQuery(".configForm").validate({
    highlight: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-error');
    }
  });

  jQuery("a.delete-row").click(function(){
    swal(
    {
        title: "Êtes-vous sure?",
        text: "Vous ne serez pas en mesure de récupérer cet élément",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, supprimez-le!",
        cancelButtonText: "Non, annuler!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm)
    {
        if (isConfirm) {
            swal("Supprimé!", "L'élement a été supprimé.", "success");
        }
        else {
            swal("Annulé", "Aucune opération n'a été effectuer", "error");
        }
    });
  });

 
  });

</script>



EOF;
?>
?>



<?php  include("layout/header.php"); ?>
<?php  include("layout/leftpanel.php"); ?>
<?php  include("layout/topmenu.php"); ?>

   <div class="pageheader">
      <h2><i class="fa fa-cogs"></i> Paramétrage <span>Journal de log</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Paramétrage</a></li>
          <li class="active">Journal de log</li>
        </ol>
      </div>
    </div>
    
   





<div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="#" class="panel-close">&times;</a>
                    <a href="#" class="minimize">&minus;</a>
                </div><!-- panel-btns -->
                <h3 class="panel-title">Journal de log</h3>
            </div>
            <div class="panel-body">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <h5 class="subtitle mb5"></h5>
                    <div class="table-responsive">
                       
                    </div><!-- table-responsive -->
                </div><!-- col-md-6 -->

 <div class="contentpanel">
              <div class="input-group">
                <h2>DU : </h2>
                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-multiple">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>

                
              <div class="input-group">
                 <h2>A : </h2>
                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-multiplee">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
              </div>
              <p>
                <br><button class="btn btn-primary btn-lg">Telecharger</button>
              </p>
               </div>
           
            </div><!-- panel-body -->
          </div><!-- panel -->
          
        </div>

                        </div>
                        
                        </div>
                    </form>


                </div><!-- col-md-6 -->

            </div><!-- row -->
            </div>
            </div> <!-- /PANEL -->

        

                   

                </div><!-- row -->
            </div>
        </div> <!-- /PANEL -->
    </div>

<?php  include("layout/rightpanel.php"); ?>
<?php  include("layout/footer.php"); ?>
